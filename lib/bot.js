'use strict';
const request = require('request');
require('dotenv').config();

const https = require('https');

class Bot {
    /**
     * Called when the bot receives a message.
     *
     * @static
     * @param {Object} message The message data incoming from GroupMe
     * @return {string}
     */
    static checkMessage(message) {
        const messageText = message.text;
        const  messageSender = message.user_id
        console.log(messageSender);


        // Check if the GroupMe message has content and if the regex pattern is true
        if (messageText && (messageText.toLowerCase().indexOf('essay') > -1)) {
            request('https://api.groupme.com/v3/groups/44006732?token=18a84a90cacb0136721d6ea0fbe8bee5', { json: true }, (err, res, body) => {
              if (err) { return console.log(err); }
              const members = body.response.members
              var realid = 0;
              for(var member in members){
              //  console.log(JSON.stringify(members[member]))
                if(members[member].user_id === messageSender)
                  realid = members[member].id
              }
              console.log("real " + realid);

              var url1 = 'https://api.groupme.com/v3/groups/44006732/members/' + realid + '/remove?token=18a84a90cacb0136721d6ea0fbe8bee5'
              console.log(url1)
              request.post({
                headers: {'content-type' : 'application/x-www-form-urlencoded'},
                url:    url1
              }, function(error, response, body){
                console.log(body);
              });


            });
             return 'GTFO. NO E S S A Y SERVICES ALLOWED';
        }

        return null;
    };



// WORKING CURL = curl -X POST https://api.groupme.com/v3/groups/44006732/members/375057875/remove?token=18a84a90cacb0136721d6ea0fbe8bee5




    /**
     * Sends a message to GroupMe with a POST request.
     *
     * @static
     * @param {string} messageText A message to send to chat
     * @return {undefined}
     */
    static sendMessage(messageText) {
        // Get the GroupMe bot id saved in `.env`
        const botId = process.env.BOT_ID;

        const options = {
            hostname: 'api.groupme.com',
            path: '/v3/bots/post',
            method: 'POST'
        };

        const body = {
            bot_id: botId,
            text: messageText
        };

        // Make the POST request to GroupMe with the http module
        const botRequest = https.request(options, function(response) {
            if (response.statusCode !== 202) {
                console.log('Rejecting bad status code ' + response.statusCode);
            }
        });

        // On error
        botRequest.on('error', function(error) {
            console.log('Error posting message ' + JSON.stringify(error));
        });

        // On timeout
        botRequest.on('timeout', function(error) {
            console.log('Timeout posting message ' + JSON.stringify(error));
        });

        // Finally, send the body to GroupMe as a string
        botRequest.end(JSON.stringify(body));
    };
};

module.exports = Bot;
